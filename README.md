# vue-japan-tour
<p>Proyecto de práctica para el desarrollo Frontend de páginas dinámicas con Vue. <br>
Simula una página informativa de un tour japones, cuenta con las siguientes secciones:
</p>
<ul>
    <li>Sección principal: Cuenta con información resumida sobre el tour y los lugares que se visitarán en el viaje.</li>
    <li>¿Qué es Vue-Japan-Tour?: Describe el servicio.</li>
    <li>¿Qué incluye?: Brinda información de todo lo que incluye el tour</li>
    <li>Lugares: Sección informativa sobre los seis lugares del tour</li>
    <li>Reservar lugar: Vista con un formulario en donde el usuario debe ingresar sus datos(Ninguno de los datos tienen persistencia    después de cerrar la página).</li>
</ul>

<h2>El proyecto cuenta con las siguientes características y funcionalidades:</h2>

<ul>
    <li>Desarrollado con Vue.js</li>
    <li>Enrutamiento de componentes</li>
    <li>Vistas con HTML y CSS</li>
    <li>Elementos dinámicos</li>
    <li>Responsive Design</li>    
    <li>Bootstrap</li>
    <li>Bootstrap-Vue</li>
</ul>

<h2>Ve el proyecto funcionando</h2>
<p>Ahora es posible visualizar la página en ejecución, previamente se ha configurado el deploy para poder ejecutar el website dinámico con la ayuda de la herramienta GitLab Pages, en conjunto con GitLab CI y GitLab Runner.</p>

### Watch online
##### Si te gustaría probar la página en ejecución has click  [Aquí](https://chichianichi.gitlab.io/vue-japan-tour  "Aquí")
