import Vue from 'vue'
import VueRouter from 'vue-router'
import MainView from '../views/MainView.vue';
import QueIncluye from '../views/QueIncluye.vue';
import Lugares from '../views/Lugares.vue';
import Formulario from '../views/Formulario.vue';
import QueEs from '../views/QueEs.vue';

Vue.use(VueRouter)

const routes = [
  {path: '/', component: MainView},
  {path: '/que-incluye', component: QueIncluye},
  {path: '/lugares/tokio', component: Lugares},
  {path: '/lugares/castillo-himeji', component: Lugares},
  {path: '/lugares/shirakawa-go', component: Lugares},
  {path: '/lugares/nara', component: Lugares},
  {path: '/lugares/monte-fuji', component: Lugares},
  {path: '/lugares/hiroshima', component: Lugares},
  {path: '/reservar-lugar', component: Formulario},
  {path: '/que-es', component: QueEs }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
